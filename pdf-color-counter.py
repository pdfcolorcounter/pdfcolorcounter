#!/usr/bin/env python

from PdfColorCounter import PdfColorCounter

import sys
import os
import argparse
import operator

def display(page):
  sys.stdout.write('Analyzed page #%d\r' % (page+1));
  sys.stdout.flush()

parser = argparse.ArgumentParser(description="PDF Color Counter: counting the number of colored pages in a PDF file")
parser.add_argument("--file", required=True, help="PDF file to analyze")
parser.add_argument("--pages", required=False, default=None, help="Comma-separated/Dash-separated list of pages to analyze")
args = parser.parse_args()

list_pages = None
if args.pages:
  # Explode a string into multiple lists
  # e.g.: 17-23,26,28,34-37 -> [[17, 18, 19, 20, 21, 22, 23], ['26'], ['28'], [34, 35, 36, 37]]
  list_pages = map(lambda x: (len(x) == 2 and range(int(x[0]), int(x[1])+1)) or (len(x) == 1 and x), map(lambda x: x.split("-"), args.pages.split(",")))
  # Flatten into one list
  # e.g.: [[17, 18, 19, 20, 21, 22, 23], ['26'], ['28'], [34, 35, 36, 37]] -> [17, 18, 19, 20, 21, 22, 23, '26', '28', 34, 35, 36, 37]
  list_pages = reduce(operator.add, list_pages)
  # Turn into 0-indexed pages numbers
  # e.g.: [17, 18, 19, 20, 21, 22, 23, '26', '28', 34, 35, 36, 37] -> [16, 17, 18, 19, 20, 21, 22, 25, 27, 33, 34, 35, 36]
  list_pages = map(lambda x: int(x)-1, list_pages)

uri = 'file://%s' % os.path.abspath(args.file)
PageCounter = PdfColorCounter(uri)
pages = PageCounter.process(pages=list_pages, callback=display)
sys.stdout.write('\rFound %d colored pages: %s\n'% (len(pages), PageCounter.pretty_pages_list()));
