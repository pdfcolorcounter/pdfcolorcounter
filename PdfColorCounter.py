import sys, os
import poppler
import cairo
import numpy

class PdfColorCounter:

  DEBUG = False
  colored = []
  doc = None
  img = None

  def __init__(self, uri):
    self.doc = poppler.document_new_from_file(uri, None)

  def process(self, pages=None, callback=None):
    pages_list = pages or range(self.doc.get_n_pages())
    for i in pages_list:
      self.produce_page(i)
      self.check_page(i)
      if callback:
        callback(i)
    return self.compute_results()

  def compute_results(self):
    return self.colored

  def pretty_pages_list(self):
    return ", ".join(map(lambda x: str(x), self.colored))

  def produce_page(self, pageNr):
    page = self.doc.get_page(pageNr)
    page_width, page_height = page.get_size()
    # Will render on a Cairo surface with black background
    self.img = cairo.ImageSurface(cairo.FORMAT_RGB24, int(page_width), int(page_height))
    context = cairo.Context(self.img)
    context.translate(0, 0)
    page.render(context)
    context.set_operator(cairo.OPERATOR_DEST_OVER)
    context.paint()
    if self.DEBUG:
      self.img.write_to_png('debug-%u.png' % (pageNr+1))

  def check_page(self, pageNr):
    buf = self.img.get_data()
    # Get bytes and make them usable in a numpy array
    a = numpy.frombuffer(buf, numpy.uint8)
    a.shape = (self.img.get_width(), self.img.get_height(), 4)
    # Filtering out black pixels
    black = numpy.array([0, 0, 0, 0])
    (filtering, indices) = (a != black).any(1).nonzero()
    has_color = len(filtering) > 0

    if has_color:
      self.colored.append(pageNr + 1)
